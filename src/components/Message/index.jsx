import React from 'react';
import PropTypes from 'prop-types';
import isMessageValid from '../../helpers/messageValidateHelper';

import edit from '../../assets/edit-solid.svg';
import trash from '../../assets/trash-alt-solid.svg';
import like from '../../assets/heart-solid.svg';
import likeRed from '../../assets/heart-red-solid.svg';

import './index.css';

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isEditing: false,
      newMessageValue: this.props.message.text
    }
  }

  getFormatedDateAndTime(inputDateAsString) {
    const inputDate = new Date(inputDateAsString)

    const time = inputDate.toLocaleTimeString();
    const date = inputDate.toLocaleDateString();

    return `${time} ${date}`;
  }

  toggleEdit() {
    this.setState({ isEditing: !this.state.isEditing })
  }

  handleChange = (event) => {
    this.setState({ newMessageValue: event.target.value });
  }

  saveChanges(id) {
    if (isMessageValid(this.state.newMessageValue)) {
      this.props.editMessage(id, this.state.newMessageValue)
    }
    this.toggleEdit()
  }

  render() {
    const { id, text, avatar, userId, createdAt, editedAt, isLiked } = this.props.message;
    return this.props.currentUser.userId === userId
      ? (
        <div className='message message-right' >
          <div className='message__content'>
            {
              this.state.isEditing
                ? <>
                  <input
                    className='message__content-input'
                    type='text'
                    value={this.state.newMessageValue}
                    onChange={this.handleChange} />
                  <button
                    className='message__content-btn'
                    onClick={() => this.saveChanges(id)}
                  >Save</button>
                </>
                : <p className='message__content-text'>{text}</p>
            }
            <span class='message__content-date'>
              {this.getFormatedDateAndTime(createdAt)} {editedAt ? `(edited: ${this.getFormatedDateAndTime(editedAt)})` : null}
            </span>
            <div className='message__content-btns'>
              <img src={edit} onClick={() => this.toggleEdit()} alt='edit' />
              <img src={trash} onClick={() => this.props.deleteMessage(id)} alt='trash' />
            </div>
          </div>
        </div>
      ) : (
        <div className='message message-left' >
          <img src={avatar} className='message__avatar' alt='avatar' />
          <div className='message__content'>
            <p className='message__content-text'>{text}</p>
            <span class='message__content-date'>
              {this.getFormatedDateAndTime(createdAt)} {editedAt ? `(edited: ${this.getFormatedDateAndTime(editedAt)})` : null}
            </span>
            <div className='message__content-btns'>
              {
                isLiked
                  ? <img src={likeRed} onClick={() => this.props.likeMessage(id)} alt='like' />
                  : <img src={like} onClick={() => this.props.likeMessage(id)} alt='like' />
              }
            </div>
          </div>
        </div >
      )
  }
}

Message.propTypes = {
  message: PropTypes.objectOf(PropTypes.any),
  currentUser: PropTypes.objectOf(PropTypes.any),
  deleteMessage: PropTypes.func,
  editMessage: PropTypes.func,
  likeMessage: PropTypes.func
}

export default Message;
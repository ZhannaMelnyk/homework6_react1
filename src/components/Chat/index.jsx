import React from 'react';
import PageHeader from '../PageHeader';
import ChatHeader from '../ChatHeader';
import MessageList from '../MessageList';
import MessageInput from '../MessageInput';
import Loader from '../Loader';
import { getData } from '../../helpers/dataHelper';
import compare from '../../helpers/sortHelper';

import './index.css';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      currentUser: {
        userId: '9e243930-83c9-11e9-8e0c-8f1a686f4ce4',
        user: "Ruth",
        avatar: "https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA",
      }
    };
  }

  componentDidMount() {
    this.checkData();
  }

  checkData() {
    getData()
      .then(response => {
        this.setState({ messages: response.sort(compare) })
      })
  }

  getUniqueUsersAmount() {
    let uniqueUsers = [];

    this.state.messages.map(message => {
      return uniqueUsers.find(value => value === message.userId)
        ? null
        : uniqueUsers.push(message.userId);
    })

    return uniqueUsers.length;
  }

  getLastMessageTime() {
    const lastMessageIndex = this.state.messages.length - 1;
    const lastMessage = this.state.messages[lastMessageIndex];

    const inputDate = new Date(lastMessage?.createdAt)
    const time = inputDate.toLocaleTimeString();

    return time;
  }

  deleteMessage = messageId => {
    const updatedMessages = this.state.messages.filter(message => message.id !== messageId)
    this.setState({ messages: updatedMessages })
  }

  createMessageId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  createMessage = messageText => {
    const newMessage = Object.assign({}, {
      id: this.createMessageId(),
      text: messageText,
      editedAt: '',
      createdAt: new Date().toISOString()
    }, this.state.currentUser)

    const updatedMessages = this.state.messages;
    updatedMessages.push(newMessage);

    this.setState({ messages: updatedMessages });
  }

  editMessage = (messageId, messageText) => {
    const updatedMessages = this.state.messages.map(message => {
      if (message.id === messageId) {
        return Object.assign({}, message, {
          text: messageText,
          editedAt: new Date().toISOString()
        })
      }
      return message;
    });
    this.setState({ messages: updatedMessages });
  }

  likeMessage = (messageId) => {
    const updatedMessages = this.state.messages.map(message => {
      if (message.id === messageId) {
        return Object.assign({}, message, {
          isLiked: !message.isLiked
        })
      }
      return message;
    });
    this.setState({ messages: updatedMessages })
  }

  render() {
    this.getLastMessageTime();
    return (
      <>
        {
          this.state.messages.length > 0
            ? <>
              <PageHeader
                userName={this.state.currentUser.user}
                avatar={this.state.currentUser.avatar} />
              <div className='chat-container'>
                <ChatHeader
                  usersAmount={this.getUniqueUsersAmount()}
                  messageAmount={this.state.messages.length}
                  lastMessageTime={this.getLastMessageTime()} />
                <MessageList
                  messageList={this.state.messages}
                  currentUser={this.state.currentUser}
                  deleteMessage={this.deleteMessage}
                  editMessage={this.editMessage}
                  likeMessage={this.likeMessage} />
                <MessageInput createMessage={this.createMessage} />
              </div>
              <footer className='footer'>
                2020 by Zhanna Melnyk
              </footer >
            </>
            : <Loader />
        }
      </>
    )
  }

}

export default Chat;
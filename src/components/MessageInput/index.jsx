import React from 'react';
import PropTypes from 'prop-types';
import isMessageValid from '../../helpers/messageValidateHelper'

import './index.css'

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = { messageText: '' }
  }

  handleChange = (event) => {
    this.setState({ messageText: event.target.value });
  }

  render() {
    return (
      <div className='chat__input-container'>
        <input
          className='chat__input'
          type='text'
          value={this.state.messageText}
          placeholder='Write a message...'
          onChange={this.handleChange} />
        <button
          className='chat__input-btn'
          onClick={() => {
            if (isMessageValid(this.state.messageText)) {
              this.props.createMessage(this.state.messageText);
              this.setState({ messageText: '' });
            }
          }} >Send</button>
      </div>
    )
  }
}

MessageInput.propTypes = {
  createMessage: PropTypes.func
}

export default MessageInput;
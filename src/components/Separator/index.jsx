import React from 'react';
import PropTypes from 'prop-types';

class Separator extends React.Component {
  render() {
    const todayDate = new Date().toDateString();
    const yesterdayDate = new Date(new Date().setDate(new Date().getDate() - 1)).toDateString();
    return (
      <div className='separator'>
        {
          (() => {
            if (this.props.date === todayDate) {
              return <span>Today</span>
            } else if (this.props.date === yesterdayDate) {
              return <span>Yesterday</span>
            } else {
              return <span>{this.props.date}</span>
            }
          })()
        }
      </div>
    )
  }
}

Separator.propTypes = {
  date: PropTypes.string
}

export default Separator;
